package listy;

public interface IChildren {

    boolean addChild(String name);

    String getChildren();

    void sort();

    void clearDuplicates();

    void namesToUpperCase();

    void namesToLowerCase();

}
