package listy;

public class Child {
    private String childName;
    private String pesel;

    public Child(String childName, String pesel) {
        this.childName = childName;
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return "Child{" +
                "childName='" + childName + '\'' +
                ", pesel='" + pesel + '\'' +
                '}';
    }

    public Child(String parameter) {
        if (isParameterValid(parameter)) {
            String[] tab = parameter.split(" ");
            childName = tab[0];
            pesel = tab[1];
        } else {
            String message = "Not valid argument: %s";
            throw new IllegalArgumentException(String.format(message, parameter));
        }
    }

    private boolean isNameValid(String name) {
        if (name == null || name.isBlank()) {
            return false;
        }

        for (int i = 0; i < name.length(); i++) {
            if (!Character.isAlphabetic(name.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private boolean isParameterValid(String parameter) {
        String[] tablica = parameter.split(" ");
        return tablica.length == 2 && isNameValid(tablica[0]) && isValidPesel(tablica[1]);
//        if (tablica.length != 2) {
//            return false;
//        }
//
//        if (!isNameValid(tablica[0])) {
//            return false;
//        }
//
//        if (!isValidPesel(tablica[1])) {
//            return false;
//        }
//
//        return true;
    }


    //to do
    private boolean isValidPesel(String pesel) {
        return true;
    }


}
