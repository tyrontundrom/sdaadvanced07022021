package listy;

public class ListChildrenMain {
    public static void main(String[] args) {

        IChildren childrenRepo = new ListChildren();

        childrenRepo.addChild("Jan");
        childrenRepo.addChild("jol-a");
        childrenRepo.addChild("kamil");
        childrenRepo.addChild("kamil");
        childrenRepo.addChild("adam");
        System.out.println(childrenRepo.getChildren());

        childrenRepo.sort();
        System.out.println(childrenRepo.getChildren());

        childrenRepo.clearDuplicates();

        System.out.println(childrenRepo.getChildren());

        childrenRepo.namesToLowerCase();
        System.out.println(childrenRepo.getChildren());

        childrenRepo.namesToUpperCase();
        System.out.println(childrenRepo.getChildren());

//        Child.isParameterValid("mariusz 3745");
//        Child.isParameterValid("batman2 3745");
//        Child.isParameterValid("anna");
//        Child.isParameterValid("3745");

        Child child = new Child("jan 123");
        System.out.println(child);


    }
}
