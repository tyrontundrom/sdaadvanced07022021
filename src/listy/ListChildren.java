package listy;

import javax.xml.crypto.dsig.spec.XSLTTransformParameterSpec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListChildren implements IChildren {
    List<String> children = new ArrayList<>();

    @Override
    public boolean addChild(String name) {

        if (isNameValid(name)) {
            return children.add(name);
        }
        return false;
    }

    private boolean isNameValid(String name) {
        if (name == null || name.isBlank()) {
            return false;
        }

        for (int i = 0; i < name.length(); i++) {
            if (!Character.isAlphabetic(name.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String getChildren() {
        return children.toString();
    }

    @Override
    public void sort() {
        Collections.sort(children);
    }

    @Override
    public void clearDuplicates() {
        List<String> tempChildren = new ArrayList<>();
        for (String name : children) {
            if (!tempChildren.contains(name)) {
                tempChildren.add(name);
            }
        }
        children = tempChildren;
    }

    @Override
    public void namesToUpperCase() {
        for (int i = 0; i < children.size(); i++) {
            children.set(i, children.get(i).toUpperCase());
        }
    }

    @Override
    public void namesToLowerCase() {
        for (int i = 0; i < children.size(); i++) {
            String tempName = children.get(i).toLowerCase();
            children.set(i, tempName);
        }
    }
}
